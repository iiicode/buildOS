# Version 0.0.1

FROM ubuntu:16.04
MAINTAINER Yunchao Chen "aaron.nehc@gmail.com"

Add sources-16.04.list /etc/apt/sources.list
RUN apt-get update
RUN apt-get install -y nginx
RUN echo "Hi, I'm in your container..." > /usr/share/nginx/html/index.html

EXPOSE 80
